class Project < ActiveRecord::Base
	belongs_to :subject

	validates :title, presence: true
	validates :creator, presence: true
	validates :image, presence: true
	validates :description, length: { maximum: 1000 }

end
