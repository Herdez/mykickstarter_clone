class Subject < ActiveRecord::Base
	has_many :projects

	validates :name, presence: true
	validates :name, length: { minimum: 2 }
end
