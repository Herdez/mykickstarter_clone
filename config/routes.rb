Rails.application.routes.draw do
  
  resources :projects, except: :index do
    member do
      get :pledge
    end
  end

  get 'projects/index'

  get 'projects/show'

  get 'projects/new'

  get 'projects/edit'

  get 'projects/:id/delete' => 'projects#delete', :as => :project_delete

  get '/subjects/:subject_id/projects/:id' => 'projects#show', as: 'project_show'

  resources :subjects
  
  get 'subjects/index'

  get 'subjects/show'

  get 'subjects/new'

  get 'subjects/edit'

  get 'subjects/:id/delete' => 'subjects#delete', :as => :subject_delete

  get 'welcome/home'

  get '/signup' => 'users#new'
  post '/users' => 'users#create'

  get '/login' => 'sessions#new'
  post '/login' => 'sessions#create'
  get '/logout' => 'sessions#destroy'

  root to: 'welcome#home'
end
